#!/usr/bin/env bash

#set -x

for i in {1..1000} ; do
   [[ $(( ${i} % 3 )) -eq 0 ]] && out='Fizz'
   [[ $(( ${i} % 5 )) -eq 0 ]] && out+='Buzz'
   [[ ! $(( ${i} % 3 )) -eq 0 ]] && [[ ! $(( ${i} % 5 )) -eq 0 ]] && out="${i}"
   echo "${out}"
   out=''
done



