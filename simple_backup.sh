#!/usr/bin/env bash


backup_data="${HOME}/backups"
[[ ! -d ${backup_data} ]] && mkdir -p ${backup_data}

function usage() {

   echo "${0} [db|ftp] ftp_path"

}

function backup::ftp() {
   backup_name=$(echo "${backup_ftp_path#/}$(date +%F_%H%m)" | tr '/' '_' )
   echo "${backup_data}/${backup_name}.gz"
   tar -zcpf ${backup_data}/${backup_name}.tar.gz ${backup_ftp_path}
   [[ $? -eq 0 ]] && echo "Success" || echo "Backup failed"

}

function main() {

   backup_type="${1}"
   backup_ftp_path="${2}"

   if [[ ${backup_type} == "ftp" ]] ; then

      [[ ! -d "${backup_ftp_path}" ]] && echo "${backup_ftp_path} doesnt exist " && usage && exit 100
      backup::ftp

   elif [[ ${backup_type} == "db" ]] ; then

      echo "Backup db "
      # todo..
   else
      usage
   fi

}

main "$@"
