#!/usr/bin/env bash

source ~/.bashrc
grouphost=$1
screenfile='/etc/screenrc' #screen file with hosts

for host in $(grep $grouphost ${screenfile} | awk '{print $NF}' | grep '@' ); 
   do 
#      echo "Checking host: ${host}" #debug
      resolver=$(ssh ${host} "systemd-resolve --status | grep 'DNS Servers' " 2>/dev/null )

      if [[ $? != 0 ]] ; then
         resolver=$(ssh ${host} "grep 'nameserver' /etc/resolv.conf")
      fi

      resolvip=$(echo ${resolver} | awk '{print $NF}' ); 

      if [[ "${resolvip}" == "10.254.32.60" ]] ; then
         echo "${host} = ${resolvip} ";
      fi 

   done

